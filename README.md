# Events Management #

### CRUD for Events ###

* Name, Description, Date & Time, Duration

### List the events ###

* Ongoing Events (ordered DESC by start)
* Future Events (ordered ASC by start)
* Past Events (ordered DESC by end)

### Statistics about events ###

* Chart with number of events organized per day during a specific month

### Authentication ###

* Administrators must authenticate in order to manage events (e-mail & password)

### Confirm participation ###

* Within an event, anyone can **participate**
* Animate **participate** action

### Synchronization ###

* When Offline: persist events & participations
* When Online: synchronize events & participations

### Contact Administrator ###

* **Intent** (send an e-mail using Gmail)
